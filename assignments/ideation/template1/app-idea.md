# Idea 
## Title
<copy title here>

## Job stories
<copy job stories here>


# Feedback 
<copy the feedback here>

# Tasks
Translate your job stories to the tasks your app will do (see previous assignment first objective where we defined the goals and tasks for the Paco's pets case). Note that the goal of the app is now described as job stories.


# Competition
Search for 3 apps that solves the same (or similar) problem your app is trying to solve. Describe the difference between your solution compared to alternatives.

# Alternative 1:
Link to website: 
- Describe how your idea is better than this alternative


- Describe in what case this alternative is better 



# Alternative 2:
Link to website: 
- Describe how your idea is better than this alternative


- Describe in what case this alternative is better 



# Alternative 3:
Link to website: 
- Describe how your idea is better than this alternative


- Describe in what case this alternative is better 

