- |
    In this assignment we will implement a simple address book. In this address book we will store some contacts with their phone numbers. The address book allows us to add, remove and search for a contact.

    Have fun programming!
-   Features:
    -   Make a menu:
        -
            text: |
                The application displays a menu and asks the user to select an option.
                ```
                *** Welcome to your blackbook ***

                You have 3 contacts in your blackbook.
                1. Display contacts
                2. Add a contact
                3. Find a contact
                4. Remove a contact
                0. Exit application
                Please choose an option: A
                ERROR: Invalid option.
                Please choose an option: 7
                ERROR: Invalid option.
                Please choose an option: -1
                ERROR: Invalid option.
                Please choose an option: _
                ```
                - Initialize the blackbook with three contacts
                - The application validates the selection made by the user. A selection is valid when:
                    - The selection is a number, and
                    - The selection (number) is between 0 and 4 (inclusive).
            ^merge: feature
            
    -   Display contacts:
        -
            link: https://realpython.com/python-dicts/
            title: Dictionaries in Python
            info: Python provides another composite data type called a dictionary, which is similar to a list in that it is a collection of objects.
        -
            text: |
                The application displays the contacts stored in the blackbook.
                ```
                *** Welcome to your blackbook ***

                You have 3 contacts in your blackbook.
                1. Display contacts
                2. Add a contact
                3. Find a contact
                4. Remove a contact
                0. Exit application
                Please choose an option: 1
                - Alice: 06 123 456 78
                - Bob: 06 321 654 87
                - Charlie: 06 876 543 21

                You have 3 contacts in your blackbook.
                1. Display contacts
                2. Add a contact
                3. Find a contact
                4. Remove a contact
                0. Exit application
                Please choose an option: _
                ```
                - Store data in a dictionary with the name as key and the phone number as value.
                - Display the index in the overview.
            ^merge: feature
            
    -   Manage your contacts:
        -
            text: |
                The application adds, finds and removes contacts from the blackbook.
                ```
                *** Welcome to your blackbook ***

                You have 3 contacts in your blackbook.
                1. Display contacts
                2. Add a contact
                3. Find a contact
                4. Remove a contact
                0. Exit application
                Please choose an option: 2
                Contact name: Timothy
                Phone number: 06 111 222 33

                You have 4 contacts in your blackbook.
                1. Display contacts
                2. Add a contact
                3. Find a contact
                4. Remove a contact
                0. Exit application
                Please choose an option: 3
                Who are you looking for: Alice
                - Alice: 06 123 456 78
                
                You have 4 contacts in your blackbook.
                1. Display contacts
                2. Add a contact
                3. Find a contact
                4. Remove a contact
                0. Exit application
                Please choose an option: 3
                Who are you looking for: Frank
                No contact found with name 'Frank'.
                
                You have 4 contacts in your blackbook.
                1. Display contacts
                2. Add a contact
                3. Find a contact
                4. Remove a contact
                0. Exit application
                Please choose an option: 4
                Who do you want to remove: Frank
                No contact found with name 'Frank'.
                
                You have 4 contacts in your blackbook.
                1. Display contacts
                2. Add a contact
                3. Find a contact
                4. Remove a contact
                0. Exit application
                Please choose an option: 4
                Who do you want to remove: Alice
                Alice removed.
                
                You have 3 contacts in your blackbook.
                1. Display contacts
                2. Add a contact
                3. Find a contact
                4. Remove a contact
                0. Exit application
                Please choose an option: _
                ```
                - Implement logic for adding a contact to the address book.
                - Implement search logic for finding a contact in the address book.
                - Implement logic for removing a contact (leverage your search function).
            ^merge: feature
    
    -   Extend the contact details:
        -
            text: |
                A proper address book not only stores one phone number but allows the user to store multiple phone numbers in for one contact. Extend you application in order to make it possible to store more phone numbers (like home, work, mobile, [fax](https://www.youtube.com/watch?v=r6OdRp4i5xY)).
                ```
                *** Welcome to your blackbook ***
                You have 3 contacts in your blackbook.
                
                1. Display contacts
                2. Add a contact
                3. Find a contact
                4. Remove a contact
                0. Exit application
                Please choose an option: 1
                - Alice: 06 123 456 78 (home)
                - Bob: 06 321 654 87 (work)
                - Charlie: 06 876 543 21 (home), 088 019 8888 (work)
                
                1. Display contacts
                2. Add a contact
                3. Find a contact
                4. Remove a contact
                0. Exit application
                Please choose an option: 2
                Contact name: Timothy
                Phone number: 06 111 222 33
                Phone number type (home, work, fax, etc): fax
                Would you like to add another phone number for this contact? [y_n] y
                Phone number: _

                ```
                - Change the data structure of you contact to support multiple phone numbers. The type of phone number could be 'home', 'work', 'mobile', 'fax' or something else entirely. *Hint*: use dictionaries (with keys like 'fax' and 'home') as the *values* of your main blackbook dictionary.
                - Update 'display contacts' to show multiple numbers and their types.
                - Update 'add a contact' to allow users to add multiple numbers.
            ^merge: feature
    
    -   Extend the search functionality:
        -
            text: |
                Extend your application to search for contacts based on their phone numbers. 
                ```
                *** Welcome to your blackbook ***

                You have 3 contacts in your blackbook.
                1. Display contacts
                2. Add a contact
                3. Find a contact
                4. Remove a contact
                0. Exit application
                Please choose an option: 3
                How do you want to search for a contact?                
                1. Search by name
                2. Search by phone number
                Please choose an option: 2
                Phone number: 0880198888
                - Charlie: 06 876 543 21 (home), 088 019 8888 (work)
                
                You have 3 contacts in your blackbook.
                1. Display contacts
                2. Add a contact
                3. Find a contact
                4. Remove a contact
                0. Exit application
                Please choose an option: 3
                How do you want to search for a contact?                
                1. Search by name
                2. Search by phone number
                Please choose an option: 2        
                Phone number: 1234
                No contact found with number '1234'.
                ```
                - Change your application to ask for the option to search for.
                - When the user chooses to search by phone number then you application should search through all (home, work, mobile and fax) phone numbers. You should also make it possible to search for similar phone numbers in different formats. For example a search for '0880198888' should return the entry *Charlie 06 876 543 21 (home), 088 019 8888 (work)*.
                - When no results are found, the application should say so.
            ^merge: feature
            
