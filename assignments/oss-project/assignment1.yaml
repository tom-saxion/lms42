- Work on an existing Open Source project.
- |
    **Note:** This is an exam *project*. As opposed to regular exams, you're allowed to:

    - Get a bit of help from the teachers.
    - Talk about the project with your class mates, and show them a demo.
    - Keep your source code, and do with it whatever you like. (Continue working on it? Impress your future employer? Build a billion dollar business around it?)

- It is recommended *not* to work on this project in a single stretch, but to alternate with the other modules. This allows you to await response to your work/proposals from the project's community.

-   Maintain a journal:
        text: |
            After each day that you worked on the project, write down the answers to the following three question:
            1. What have you accomplished today?
            2. What do you know now what you did not know this morning?
            3. What are the questions you (still) have at the moment?

            At the end of the project submit your journal. To get an idea of how elaborate your answers should be: aim for 100+ words per day (for the three answers combined).
        map: legacy_analyze
        0: No relevant questions have been posed during the execution of the project.
        2: Some relevant question have been posed or many question have been posed only some answered.
        4: Relevant questions have been posed and properly answered (by the student) during the execution of the project.

-   Implement your feature(s) and/or fixes:
        map: legacy_impl
        1: 1 out of 4
        2: 2 out of 4
        3: 3 out of 4
        4: Complex, substantial, well-executed and independent.
        text: |
            You get points for implementing a solution (or a set of solutions) that is substantial in size and (inherent) complexity. Besides that, code quality and the level of independence calculate into this grade.

            You can pick up more (and larger) issues until you feel you've made a reasonable sized contribution in total. No need to overdo it though! Talk to a teacher is case of doubt.

-   Adhere to the projects coding standards and contribute upstream:
        map: legacy_contrib
        0: Coding standards and contribution guidelines were not observed.
        2: Coding standards and contribution guidelines were partly observed.
        4: Coding standards and contribution guidelines were fully observed, and the student demonstrated a collaborative attitude.
        text: |
            Note that your patch(es) don't need to be accepted (yet) in order to receive a good grade. You *should* however have taken all the appropriate steps to do your part in achieving this, demonstrating a collaborative attitude and respecting other people's time.

-   Pitching:
        map: presentation
        scale: 10
        text: |
            You must have successfully completed your *Five minute pitch* (or the *Presentation* assignment that used to be part of the *Software teams* module) before submitting this assignment.

-   Submitting:
        text: |
            Your LMS submission should contain:
            
            - URLs for the Pull/Merge Request(s) and (possibly) issue threads that you participated in.
            - A patch (also know as a *diff*) for each of your Pull Requests.
            - Your journal.
        must: true
