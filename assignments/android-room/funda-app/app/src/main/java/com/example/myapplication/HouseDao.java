package com.example.myapplication;

import androidx.annotation.NonNull;
import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;

import com.example.myapplication.models.Bid;
import com.example.myapplication.models.House;

import java.util.List;

@Dao
public interface HouseDao {
    @Query("select * from house")
    LiveData<List<House>> getAllHouses();

    public class HouseWithBids extends House {
        int bids;
        int highestBid;

        @NonNull
        @Override
        public String toString() {
            return streetName + " " + houseNumber + " (" + size +" m2, €" + askingPrice + ", " + bids + " bids, highest bid "+highestBid+")";
        }
    }

    @Query("select house.*, count(bid.houseId) as bids, max(bid.amount) highestBid from house left join bid on house.id=bid.houseId group by house.id")
    LiveData<List<HouseWithBids>> getAllHousesWithBids();

    @Query("select * from house where id=:id")
    LiveData<House> getHouse(long id);

    @Insert
    void addHouse(House house);

    @Insert
    void addBid(Bid bid);
}
