#!/bin/env python3

import sys
import yaml
import os
import subprocess
LMS_DIR = os.path.join(os.path.dirname(__file__), '..')
sys.path.append(LMS_DIR)

from lms42.app import db, app
from lms42.models import keyvalue
from lms42.models.attempt import Attempt
from lms42.models.feedback import NodeFeedback

if len(sys.argv) == 2 and sys.argv[1] == '--update-db':

    processed = keyvalue.get("renames_processed") or 0

    with open("assignments/renames.yaml") as file:
        renames = yaml.load(file, Loader=yaml.FullLoader) or []

    for index, item in enumerate(renames):
        assert len(item) == 1
        if index < processed:
            continue
        old, new = next(iter(item.items()))
        print(f"Renaming {old} --> {new}")

        Attempt.query.filter_by(node_id=old).update({'node_id': new}, synchronize_session=False)
        NodeFeedback.query.filter_by(node_id=old).update({'node_id': new}, synchronize_session=False)
        keyvalue.set("renames_processed", index+1)
        db.session.commit()

        for attempt in Attempt.query.filter_by(node_id=new, data_deleted=False):
            old_dir = os.path.join(app.config['DATA_DIR'], 'attempts', f"{old}-{attempt.student_id}-{attempt.number}")
            os.rename(old_dir, attempt.directory)


elif len(sys.argv) == 3 and sys.argv[1][0] != '-':
    old = sys.argv[1]
    new = sys.argv[2]
    # Move the directory
    old_path = f"{LMS_DIR}/assignments/{old}"
    new_path = f"{LMS_DIR}/assignments/{new}"
    if not os.path.isdir(old_path):
        raise FileNotFoundError(f"{old_path} does not exist")
    if os.path.isdir(new_path):
        raise FileExistsError(f"{new_path} already exists")
    os.rename(old_path, new_path)

    # Add an entry to the renames file
    with open(LMS_DIR+"/assignments/renames.yaml", "a") as file:
        file.write(f"- {old}: {new}\n")

    # Update the assignment reference in modules/
    subprocess.run(f"find '{LMS_DIR}/modules' -type f -exec sed -i 's/^\\(\\s*\\)- {old}/\\1- {new}/g' {{}} +", shell=True)

else:
    print(f"Usage: {sys.argv[0]} OLD_NAME NEW_NAME | --update-db")
