"""empty message

Revision ID: ea50d47ae0f7
Revises: 0052e5d77869
Create Date: 2024-02-01 14:38:29.033325

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = 'ea50d47ae0f7'
down_revision = '0052e5d77869'
branch_labels = None
depends_on = None


def upgrade():
    op.add_column('user', sa.Column('discord_notification_same_exercise', sa.Boolean(), nullable=False, server_default="true"))
    op.add_column('user', sa.Column('discord_notification_exam_reviewed', sa.Boolean(), nullable=False, server_default="true"))


def downgrade():
    op.drop_column('user', 'discord_notification_exam_reviewed')
    op.drop_column('user', 'discord_notification_same_exercise')
